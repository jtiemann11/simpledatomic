#Description

A frontend to the simple dataomic server. [https://bitbucket.org/jtiemann11/simpledatomicserver]

Ajax driven api calls to clojure-datomic backend

Fun with three.js. [requestAnimationFrame rocks!]

#Startup
from datomic directory
bin/transactor config/dev-transactor.properties

from contact directory
lein ring server, lein autoexpect (if you want to auto exec test)

Configs are:
lein autoexpect in ~/.lein/profiles.clj

{:user {:plugins [[lein-autoexpect "1.0"]]}}

db uri (def uri  "datomic:free://localhost:4334/contacts-db")

Place this repo on a web server (same machine as datomic and ring)

Play with it.  All events are either on click or on left click. 

* "Create Group" button --  self explanetory
* "Orbit Camera Control" -- when turned on (click on sub-button), allows drag and rotate of cubes world
*  Click and right click on Labels to filter, right click on labels to add, right click on note to delete 



# [HTML5 Boilerplate](http://html5boilerplate.com)

HTML5 Boilerplate is a professional front-end template for building fast,
robust, and adaptable web apps or sites.

This project is the product of many years of iterative development and combined
community knowledge. It does not impose a specific development philosophy or
framework, so you're free to architect your code in the way that you want.

* Source: [https://github.com/h5bp/html5-boilerplate](https://github.com/h5bp/html5-boilerplate)
* Homepage: [http://html5boilerplate.com](http://html5boilerplate.com)
* Twitter: [@h5bp](http://twitter.com/h5bp)
